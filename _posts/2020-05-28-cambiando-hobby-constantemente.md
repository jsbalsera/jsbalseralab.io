---
layout: post
title: "Cambiando de hobby constantemente"
author: jsbalsera
categories: [ Personal ]
image: assets/images/posts/2.jpg
excerpt: "Por qué sé un poco de muchas cosas, en vez de mucho de pocas."
---
¿Recordáis cuando aprendisteis a conducir, esas primeras veces detrás de un volante? Supongo que, como yo, necesitabais poner toda vuestra atención en lo que estabais haciendo, en cada detalle. Decidir conscientemente cuando pisabais cada pedal, cuando soltabais el embrague para ir acelerando. Pero, según pasaba el tiempo, y os acostumbrabais a conducir, esta actividad requería menos atención consciente y, hoy día, podéis hacer otras cosas mientras manejar el coche queda en segundo plano.

Una de las cosas que he descubierto con los años es que no me gusta dejar que lo que esté haciendo en ese momento pase a segundo plano. O, más bien, que necesito constantemente hacer algo que requiera dedicación completa. Ya cuando estudiaba en la universidad me di cuenta de que, si seguía todos los días el mismo camino hacia la estación, la mayoría de las veces ni siquiera recordaba cómo había llegado a clase. El piloto automático tomaba el control, no había nada que requiriera que me concentrara en ello y, simplemente, ese tiempo prácticamente desaparecía.

Una vez empecé a trabajar descubrí que no conseguía dejar de pensar en lo que en ese momento me preocupara del trabajo, tan solo cuando leía. Llegar a casa y ver series o películas, o jugar a la consola, me bastaba para entretenerme y descansar físicamente, pero mentalmente seguía agotado. Pero por suerte, en algún momento, me di cuenta de que dedicarme a algo nuevo, algo que requiera destreza manual y mental, impedía que el runrun de mi cabeza se mantuviera en los problemas laborales, consiguiendo un tipo de descanso necesario que hasta entonces me evadía.

Es por eso que cada cierto tiempo busco una nueva afición, o recupero una antigua, y me sumerjo en ella por completo. Ayuda que mi personalidad un tanto obsesiva me predisponga a intentar absorber todo el conocimiento posible en poco tiempo, claro.

Así que ya sabéis, si notáis que os cuesta desconectar, buscad una guitarra o un ukelele y unos vídeos de Youtube. O aprended a tejer, o a patinar. O a conducir, claro :D


Foto de [Austin Neill](https://unsplash.com/@arstyy?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) en [Unsplash](https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
