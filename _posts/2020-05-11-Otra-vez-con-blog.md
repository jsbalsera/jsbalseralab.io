---
layout: post
title:  "Otra vez con blog"
author: jsbalsera
categories: [ Personal ]
image: assets/images/posts/1.jpg
excerpt: "Tras muchos años sin blog vuelvo a intentarlo en otra etapa, con un nombre distinto, aunque al final volverá a ser más de lo mismo."
---

Yo solía tener un blog. O varios,de hecho, aunque no a la vez. Estuve en varios sitios, siempre con Wordpress, creo. Sin embargo acabé perdiendo el contenido y la base de datos del último hosting en el que estuve, en el que igualmente ya apenas nunca escribía y, aunque me dio pena, en cierto modo fue casi un alivio.

Cuando apareció Medium pensé en escribir aunque solo fuera un post, pero la verdad es que nunca encontré un tema que motivara lo suficiente para dedicarle el tiempo necesario. Según pasó el tiempo, además, me fue gustando menos aun la plataforma, incluso como lector. Da la sensación de que el contenido no es de quien lo escribe, sino de la plataforma.

Un tiempo después descubrí [Gatsby](https://www.gatsbyjs.org/) y he de reconocer que me gustó mucho la herramienta, y la posibilidad de tener un blog hecho con páginas estáticas empezó a parecerme muy atractiva, y quizás era una buena excusa para aprender a trastear con React, y para por fin usar Drupal para un proyecto propio. Sin embargo, de nuevo, nunca era el momento adecuado, y en este caso sabía que para tenerlo todo a mi gusto iba a necesitar una cantidad de tiempo considerable.

El nacimiento de mi hija, hace ya más de dos años, me impulsaba a querer tener algún sitio donde anotar reflexiones y pensamientos, cosas que solo me importan a mi y, tal vez, si hago las cosas un poco bien, también a ella en algún momento. Aunque siendo sincero probablemente solo sirvan para que yo mismo me avergüence al releer, si es que alguna vez lo hago. Con suerte al menos mi mujer no sufrirá mucho cuando le pida que lo lea.

Y así llegamos a los últimos meses. Mi amigo [Alvaro Hurtado](https://www.alvar0hurtad0.es/) comentó la idea de publicar él mismo un blog, de una manera sencilla y funcional, y fue el último impulso que necesitaba. Quizás en otra entrada comentaré como de simple es montar un blog de esta manera y, aun así, he tardado meses. Pero ha sido complicado renunciar a tenerlo todo como yo quería. Por ejemplo, sale a la luz con Disqus como sistema de comentarios, cuando yo no quería usar servicios de terceros ni que se carguen bibliotecas externas, pero es lo que hay (por ahora). Y me hubiera gustado prepararlo para poder publicar también en inglés. Pero al final hemos de aceptar la realidad:

> Hecho es mejor que perfecto

Como me dijo Álvaro. O, como lo reformuló [Nacho](http://www.isholgueras.com/):

> Lo mejor es enemigo de lo bueno

Así que no sé cuánto escribiré, ni si esto durará. Es una nueva etapa (creo que la quinta), y llevaba sin publicar, según Feedburner, desde Mayo de 2014. Desde 2012 sin regularidad. Pero bueno, mientras dure la racha, escribiré y publicaré. Aunque solo sea por decir algo (online).

Foto de [Jason Leung](https://unsplash.com/@ninjason?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) en [Unsplash](https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)