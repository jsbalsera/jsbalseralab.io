---
layout: post
title: "Podcast #002: David Hernández, Servicios gratuitos, GAFAM, y los datos que les damos"
categories: [Podcast]
author: jsbalsera
image: assets/images/podcasts/2.png
excerpt: "En este segundo episodio del Podcast charlamos con David Hernández, y recordamos qué damos a las grandes empresas cuando usamos sus servicios."
podcast_tags: [mail, cloud, privacidad, gafam]
podcast_link: "https://pordeciralgoonline.s3.eu-west-3.amazonaws.com/PDAO002.mp3"
duration: 2668
length: 96103308
season: 1
episode: 2
episodeType: full
---
Segundo episodio de este proyecto de podcast, hablando esta vez con David Hernández. En este caso estuvimos charlando mucho rato sin darnos cuenta, por lo que ha quedado bastante más largo.

Este segundo episodio lo grabamos usando la instancia de Nextcloud de David, con un plugin para videollamadas, en vez de Zoom como en el anterior, que siempre es bueno usar herramientas libres. Intenté mejorar el sonido, usando Jack Mixer para nivelar las entradas de audio, y usando ciertos plug-ins de audio antes de grabar, pero creo que fue peor aun, así que tocará seguir investigando y trabajando en ello.

Esta vez he añadido una especie de editorial, en la que doy mi opinión sobre un tema de actualidad. Le he llamado `La chapa de hoy`, aunque espero que no resulte tan malo :D Aun no estoy convencido de que este sea su sitio, y quizás fuera mejor escribir sobre esos temas en entradas de blog, tendré que pensarlo.

La canción de inicio y fin de la entrevista fue elegida por David, creo que le va muy bien a la temática del podcast.

* 0:00 Introducción.  
* 1:35 Inicio de entrevista.  
* 42:15 Cierre.

**Enlaces:**

* Página de David: [http://davidhernandez.info/](http://davidhernandez.info/)
* Podcast Leyendo Ciencia-Ficción: [https://leyendocienciaficcion.ivoox.com/](https://leyendocienciaficcion.ivoox.com/)
* Quit Gafam: [https://quitgafam.com/](https://quitgafam.com/) 
* Vídeo oficial de [Space Oddity](https://www.youtube.com/watch?v=iYYRH4apXDo) de David Bowie en Youtube.  
* [Home Studio Libre](https://homestudiolibre.fandom.com/wiki/Home_Studio_Libre_Wiki)  

**Música de este episodio:**  
* <u>Intro</u>: Protofunk by Kevin MacLeod
Link: [https://incompetech.filmmusic.io/song/4247-protofunk](https://incompetech.filmmusic.io/song/4247-protofunks)  
License: [http://creativecommons.org/licenses/by/4.0/](http://creativecommons.org/licenses/by/4.0/)

* <u>Inicio y fin de la entrevista</u>: VIOLÈNCIA del grupo ORXATA
Vídeo oficial de [VIOLÈNCIA](https://www.youtube.com/watch?v=FBAj_qD2uFY) en Youtube
[https://orxata.tv/track/viol-ncia](https://orxata.tv/track/viol-ncia)  
Creative Commons Attribution 3.0-NonCommercial-ShareAlike Unported License
[https://creativecommons.org/licenses/by/3.0/deed.en_US](https://creativecommons.org/licenses/by/3.0/deed.en_US)

* <u>Despedida</u>: Boat by Vlad Gluschenko  
[https://www.free-stock-music.com/vlad-gluschenko-boat.html]
Boat by Vlad Gluschenko | [https://soundcloud.com/vgl9](https://soundcloud.com/vgl9)
Music promoted by [https://www.free-stock-music.com](https://www.free-stock-music.com)
Creative Commons Attribution 3.0 Unported License
[https://creativecommons.org/licenses/by/3.0/deed.en_US](https://creativecommons.org/licenses/by/3.0/deed.en_US)