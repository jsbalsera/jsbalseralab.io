---
layout: post
title: "Podcast #001: Alvaro Hurtado, los blogs y la importancia de los datos"
categories: [Podcast]
author: jsbalsera
image: assets/images/podcasts/1.png
excerpt: "En este primer episodio del Podcast charlamos con Álvaro Hurtado, y repasamos por qué nos creamos un blog en pleno 2020."
podcast_tags: [blog, podcast, privacidad, gdpr]
podcast_link: "https://pordeciralgoonline.s3.eu-west-3.amazonaws.com/PDAO001.mp3"
duration: 1931
length: 49257325
season: 1
episode: 1
---
Con esto por fin publico el primer episodio de este nuevo proyecto, que no sé si para algunos será sorpresa, pero imagino que el logo del blog ayudaba a imaginárselo. Es un podcast muy cortito, apenas media horita, y la mayor parte se la lleva en una interesante conversación con Álvaro.

La calidad del podcast, tanto a nivel de locución como de grabación y edición, está muy por debajo de lo que esperaba, pero ya haré un post sobre eso en particular espero que durante la semana.

* 0:00 Introducción.  
* 2:35 Inicio de entrevista.  
* 29:28 Cierre.

**Enlaces:**

* Blog de Alvaro Hurtado: [https://www.alvar0hurtad0.es/](https://www.alvar0hurtad0.es/)  
* El documento que menciono sobre GDPR: [Guidelines 05/2020 on consent under Regulation 2016/679](https://edpb.europa.eu/our-work-tools/our-documents/guidelines/guidelines-052020-consent-under-regulation-2016679_es)  
* Canción de [Let it Go](https://www.youtube.com/watch?v=YVVTZgwYwVo) de Idina Menzel en versión original.  
* Canción de [Puffin Rock](https://www.youtube.com/watch?v=_Mc1i8fD4VE) en versión original (aunque la que cantamos en casa sea en castellano, pero no la encuentro.)  

**Música de este episodio:**  
* <u>Intro</u>: Protofunk by Kevin MacLeod
Link: [https://incompetech.filmmusic.io/song/4247-protofunk](https://incompetech.filmmusic.io/song/4247-protofunks)  
License: [http://creativecommons.org/licenses/by/4.0/](http://creativecommons.org/licenses/by/4.0/)

* <u>Inicio de la entrevista</u>: Inspiring Optimistic Upbeat Energetic Guitar Rhythm by Free Music
[https://www.free-stock-music.com/fm-freemusic-inspiring-optimistic-upbeat-energetic-guitar-rhythm.html](https://www.free-stock-music.com/fm-freemusic-inspiring-optimistic-upbeat-energetic-guitar-rhythm.html)  
Inspiring Optimistic Upbeat Energetic Guitar Rhythm by Free Music | [https://soundcloud.com/fm_freemusic](https://soundcloud.com/fm_freemusic)  
Music promoted by [https://www.free-stock-music.com](https://www.free-stock-music.com)  
Creative Commons Attribution 3.0 Unported License
[https://creativecommons.org/licenses/by/3.0/deed.en_US](https://creativecommons.org/licenses/by/3.0/deed.en_US)

* <u>Final de la entrevista</u>: Life Blossom by Keys of Moon  
[https://www.free-stock-music.com/keys-of-moon-life-blossom.html]  
Life Blossom by Keys of Moon | [https://soundcloud.com/keysofmoon](https://soundcloud.com/keysofmoon)  
Music promoted by [https://www.free-stock-music.com](https://www.free-stock-music.com)  
Attribution 4.0 International (CC BY 4.0)
[http://creativecommons.org/licenses/by/4.0/](http://creativecommons.org/licenses/by/4.0/)

* <u>Despedida</u>: Boat by Vlad Gluschenko  
[https://www.free-stock-music.com/vlad-gluschenko-boat.html]
Boat by Vlad Gluschenko | [https://soundcloud.com/vgl9](https://soundcloud.com/vgl9)
Music promoted by [https://www.free-stock-music.com](https://www.free-stock-music.com)
Creative Commons Attribution 3.0 Unported License
[https://creativecommons.org/licenses/by/3.0/deed.en_US](https://creativecommons.org/licenses/by/3.0/deed.en_US)