---
layout: post
title: "Hasta la próxima primavera"
author: jsbalsera
categories: [ Personal ]
image: assets/images/posts/4.jpg
excerpt: "Una niña, sus abuelos y una pandemia de por medio."
---
Durante las últimas semanas mis padres han cuidado a mi hija mientras mi mujer y yo trabajábamos, tras muchos meses de relacionarse por videollamada. El apego que existe entre ellos es evidente: cuando ella nació estuvimos varios meses en su casa, debido a ciertos imprevistos, hasta que encontramos un piso al que mudarnos. 

Desde entonces y hasta que cumplió su primer añito, todas las mañanas empezaban llevándola en coche con ellos y, cuando ya empezó la guardería, eran mis padres quienes la recogían hasta que podíamos ir a por ella tras el trabajo.

Entiendo que nada de eso os sorprende a los que tenéis hijos, es sabido que durante mucho tiempo los niños pasan más tiempo con sus abuelos que con cualquier otra persona, y se crean unos lazos especiales. Pero sé que mis padres la han echado mucho de menos estos meses, a ella y al resto de sus nietos.

El que se hicieran cargo de cuidarla ha significado que hemos tenido que protegernos mucho más, puesto que la niña no usa mascarilla aún, y no queríamos que a través de ella mis padres, ya en edad de riesgo, sufrieran un contagio. Hemos vivido casi como durante el confinamiento, sin apenas ver a nadie y reduciendo por completo nuestra vida social, intentando ser precavidos y responsables.

Eso se acaba mañana. Y es que, tras muchísimos días, mañana vuelve a funcionar la guardería, y mi hija volverá a jugar con niños y niñas de su edad, que también lo necesita. Y aunque solamente serán ocho o nueve en su clase, sin mezclarse con otros niños o profesores, pensamos que es un riesgo demasiado grande, por lo que tras la guarde vendrá a casa.

Por eso hoy cuando hemos salido del ascensor y y mi madre como siempre esperaba asomada al último tramo para decir adiós a su nieta, hemos hablado de próximos paseos con mascarilla y respetando distancias de seguridad. Y me ha dicho que con suerte quizás en primavera puedan volver a recoger ellos a la niña de la guardería. Y yo he tenido que irme, para evitar que viera el nudo que se me formaba en la garganta.

Foto de [Charles Tyler](https://unsplash.com/@charlietylers?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) en [Unsplash](https://unsplash.com/s/photos/spring-winter?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
