---
layout: post
title: "Problemas logísticos"
author: jsbalsera
categories: [ Personal ]
image: assets/images/posts/3.jpg
excerpt: "Por qué no ha salido el siguiente episodio del Podcast ni he escrito más"
---
Varias personas me han preguntado qué ha pasado con el podcast, visto que los dos primeros episodios salieron casi consecutivamente y no he vuelto a publicar nada. Aparte de eso, el blog tampoco ha tenido movimiento. ¿Qué ha pasado?

Lo primero deciros que la parte principal del podcast, una conversación con [Samuel Solís](https://twitter.com/estoyausente), fue grabada a principios de Julio, aunque por distintos motivos personales ya sabía que editarlo no sería sencillo por un tiempo. También grabé una entradilla, que posteriormente tuve que descartar, y hace poco volví a grabar otra entradilla y un cierre, y hasta los edité, pero creo que tampoco servirán. Así que antes de nada disculpas a Samu, que aceptó charlar conmigo pero esto no termina de salir a la luz.

Y es que cuando empecé a tener algo más de tiempo libre de nuevo mi portátil personal, el que utilizo para grabar y editar, dejó de funcionar: el cargador ya no funcionaba. Pedí un nuevo, compatible, que tan solo me permite arrancarlo pero no cargarlo, y encima el conector que se sale con cierta facilidad. Y la verdad es que eso impacta mucho mi proceso de edición. Para que os hagáis una idea, en el último que fue publicado dediqué 5 ó 6 horas en la edición del podcast y la composición de las notas de texto que lo acompañan. Para ser capaz de hacerlo estuve utilizando los pequeños huecos que iba teniendo, y eso significa que a ratos editaba en el sofá, a ratos incluso en la cama y, finalmente, sacrifiqué horas de sueño una noche para terminarlo todo. 

Mientras tanto he aprovechado para hacer otras cosas, como modernizar mis conocimientos de Javascript (incluyendo hacer algunas katas en [Codewar](https://www.codewars.com/)), y dedicarle algo de tiempo a indagar sobre los frameworks modernos (React, Vue y mi favorito Svelte), aunque eso merece un post propio. He visto algunas películas en incluso he vuelgo a jugar a la PS4, de forma sorprendente.

Espero ser capaz de editar el podcast pendiente pronto, y tengo otros cuantos episodios programados pero, aparte de eso, quiero retomar el blog y escribir más artículos.

Nos leemos, y oímos, pronto.

Foto de [Alistair MacRobert](https://unsplash.com/@alistairmacrobert?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyTex) en [Unsplash](https://unsplash.com/?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText)
